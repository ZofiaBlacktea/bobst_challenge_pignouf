﻿using System;
using System.Net;
using System.Net.Sockets;
using Tobii.Interaction;
using System.Threading;

namespace EyeTrackerServer {

	public class Program {
		public static void Main( string[] args ) {
		
			// Initialize the Eyetracking
			EyeDataSource source = new EyeDataSource();
			source.initialize();

			// Extract the shared memory between the server and the source
			EyeData shared_data = source.get_shared_data();
			
			// Create TCP server to stream data, default 127.0.0.1:8080
			EyeDataServer tcp = new EyeDataServer();

			// Bind the server with the shared data
			tcp.set_shared_data(shared_data);

			// Launch server
			tcp.start();

			// Press key to kill the process
			Console.ReadKey();
			
			// Cleanup
			tcp.stop();
			source.stop();
		}
	}

	class EyeDataSource {
		Host host = null;
		EyeData data = null;
		GazePointDataStream gazePointDataStream = null;

		public EyeDataSource() { }

		public void initialize() {
			data = new EyeData();

			// Initialize stream
			host = new Host();
			gazePointDataStream = host.Streams.CreateGazePointDataStream();

			// Bind stream to the shared object
			gazePointDataStream.GazePoint( ( x, y, ts ) => data.set_frame( ts, x, y ) );
		}

		public EyeData get_shared_data() { return data; }

		public void stop() { host.DisableConnection(); }
	}

	class EyeData {
		protected double x;
		protected double y;
		protected double time_stamp;

		public EyeData(){ }

		public void set_frame(double time_stamp, double x, double y) {
			this.time_stamp = time_stamp;
			this.x = x;
			this.y = y;
		}

		public void get_frame(out double time_stamp, out double x, out double y ) {
			time_stamp = this.time_stamp;
			x = this.x;
			y = this.y;
		}

		public string get_info() {
			if(y < 0 || y > 1028) return "h";
			if(x < 0 || x > 3840) return "h";

			if(x <= 1545) return "l";
			if(x >= 1545 && x <= 2842) return "m";
			if(x >= 2842) return "r";

			return "h";
		}

	}
	
	class EyeDataServer {

		//===============//
		// Instance vars //
		//===============//
		int port = 8080;
		TcpListener server = null;
		IPAddress listening_address = IPAddress.Parse( "127.0.0.1" );
		EyeData shared_data = null;
		Thread listening_thread = null;
		bool is_running = false;
		

		public EyeDataServer() { }

		//===========//
		// Accessors //
		//===========//
		public void set_shared_data(EyeData shared_data ) { this.shared_data = shared_data; }
		public void set_port( int port ) {
			if( port < 0 || port > 68000 ) throw new Exception("Bad port specified !");
			this.port = port;
		}
		public void set_ip( string ip) { try { listening_address = IPAddress.Parse( ip ); } catch { throw new Exception("Bad IP specified !"); } }


		//========//
		// Method //
		//========//
		public void start() {
			Console.Write("Initialize the server\n");
			try {
				is_running = true;
				server = new TcpListener( listening_address, port );
				server.Start();
				listening_thread = new Thread(new ThreadStart(listen));
				listening_thread.Start();
				Console.Write("Server started !\n");
			} catch {
				server.Stop();
				server = null;
				is_running = false;
				throw new Exception("Unable to launch the server !");			
			}
		}

		public void stop() { server.Stop(); }


		//========//
		// Thread //
		//========//
		protected void listen(){
			if(server == null) throw new Exception("Cannot listen connections if the server is not launched !");
			Byte[] bytes = new Byte[1024];
			int i = 0;

			while ( is_running ) {
				Console.Write( "Listening for connection\n" );
				TcpClient client = server.AcceptTcpClient();

				Console.WriteLine( "Client connected !\n" );
				NetworkStream stream = client.GetStream();
				
				// Loop to receive all the data sent by the client.
				while ( is_running && ( i = stream.Read( bytes, 0, bytes.Length ) ) != 0 ) {
					/*
					// Translate data bytes to a ASCII string.
					data = System.Text.Encoding.ASCII.GetString( bytes, 0, i );
					Console.WriteLine( "Received: {0}", data );.
					*/

					byte[] msg = System.Text.Encoding.ASCII.GetBytes( shared_data.get_info() );

					// Send the answer
					stream.Write( msg, 0, msg.Length );
					Console.Write( "TX : {0}\n",  shared_data.get_info() );
				}

				// Shutdown and end connection
				client.Close();
			}
		}
	}

}
