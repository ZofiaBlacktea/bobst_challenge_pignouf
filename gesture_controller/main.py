###########
# Imports #
###########
import requests
import socket
import gesture


########
# Main #
########
def main():
	host = "127.0.0.1"
	port = 8080
	
    # Create a sample listener and controller
    listener = SampleListener()
    controller = Leap.Controller()

    # Have the sample listener receive events from the controller
    controller.add_listener(listener)

    # Keep this process running until Enter is pressed
    print "Press Enter to quit..."
    try:
        sys.stdin.readline()
    except KeyboardInterrupt:
        pass
    finally:
        # Remove the sample listener when done
        controller.remove_listener(listener)





# %%
machine = BobstApi(https=False)


def update_machine_state(hand_state, sock):

    sock.send("check_login")
    area = sock.recv(4096)

    if hand_state.leftHandState == "reset" and hand_state.rightHandState == "reset":
        return machine.reboot()
		
    elif (hand_state.leftHandState == "reset" and hand_state.rightHandState != "reset") or (hand_state.leftHandState != "reset" and hand_state.rightHandState == "reset"):
        return machine.reset()
		
    elif hand_state.leftHandState == "login" or hand_state.rightHandState != "login":
        return machine.login()
		
    elif (hand_state.leftHandState == "positive" and hand_state.rightHandState != "positive") or (hand_state.leftHandState != "positive" and hand_state.rightHandState == "positive") and area == "m":
        return machine.set_speed(up=True)
		
    elif (hand_state.leftHandState == "negative" and hand_state.rightHandState != "negative") or (hand_state.leftHandState != "negative" and hand_state.rightHandState == "negative") and area == "m":
        return machine.set_speed(up=False)
		
    elif hand_state.leftHandState == "positive" or hand_state.rightHandState == "positive" and area == "l":
        return machine.fix_issue()
		
    elif hand_state.leftHandState == "negative" or hand_state.rightHandState == "negative" and area == "l":
        return machine.empty_delivery()
		
    elif hand_state.leftHandState == "positive" or hand_state.rightHandState == "positive" and area == "r":
        return machine.set_feeder(action="start")
		
    elif hand_state.leftHandState == "negative" or hand_state.rightHandState == "negative" and area == "r":
        return machine.set_feeder(action="stop")
		
    elif hand_state.leftHandState == "push" or hand_state.rightHandState == "push" and area == "r":
        return machine.set_feeder(action="feed")
		
    elif hand_state.leftHandState == "maxpeed" and hand_state.rightHandState == "maxpeed" and area == "m":
        # return machine.set_feeder(action="stop")
        pass
		
    elif hand_state.leftHandState == "minspeed" and hand_state.rightHandState == "minspeed" and area == "m":
        pass







if __name__ == "__main__":
    main()





