###########
# Imports #
###########
import os, sys, inspect, thread, time
import numpy as np

src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
# Windows and Linux
arch_dir = './import/' if sys.maxsize > 2**32 else '../lib/x86'

# Mac
#arch_dir = os.path.abspath(os.path.join(src_dir, '../lib'))

sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))

sys.path.append("../resources/LeapDeveloperKit_2.3.1+31549_win/LeapSDK/lib/x64")
sys.path.append("../lib/")


import Leap
import BobstController


#import ../interface.py




#push towards the left with right hand = feed feeder
#one closed hand for 1 second = reset
#both hands closed for 1 second = reboot



#how long should a hand stay closed to be detected? = time (seconds) * estimated fps
CONST_CLOSEDLENGTH = 1 * 100
#how closed should the hand be to be considered closed?
CONST_CLOSEDRADIUS = 40
#the horizontal direction
CONST_HORIZONTAL = [0, 0, -1]



class HandState():
    leftHandState = None
    rightHandState = None

class SampleListener(Leap.Listener):
    #Francesca
    
	
	
    startCounting = 0
    closedHandCounter = 0
    rightHand = None
    howManyClosedHands = 0
    handClosedR = 0
    handClosedL = 0
    startCountingBoth = 0
    closedBothHandsCounter = 0
    insideApp = False

    normal = [None, None, None]
    velocity = [None, None, None]
    rightHandHorizontal = 0

    def on_connect(self, controller):
        print "Connected"


    def on_frame(self, controller):
        handState = HandState()
        
        frame = controller.frame()
        #Thibault
        handList=frame.hands
        epsilon=500
        tip=handList.rightmost.fingers[0]

        fingerTab= [ tip, tip, tip, tip, tip]

        if self.insideApp == False:

            for hand in handList:
                finger_on_hand = hand.fingers
                
                for finger in finger_on_hand:
                    if finger.type==0:
                        fingerTab[0]=finger
                    elif finger.type==1:
                        fingerTab[1]=finger
                    elif finger.type==2:
                        fingerTab[2]=finger
                    elif finger.type==3:
                        fingerTab[3]=finger
                    elif finger.type==4:
                        fingerTab[4]=finger
                


                if (fingerTab[1].direction[2] < -0.7
                    and fingerTab[0].direction[2] < -0.4
                    and fingerTab[2].direction[2] > -0.1 and fingerTab[3].direction[2] > -0.1
                    and fingerTab[4].direction[2] > -0.1):
                    print "Start Link"
                    self.insideApp = True
                    if(hand.is_right):
                        handState.rightHandState = "login"
                        handState.leftHandState = None
                    elif(hand.is_left):
                        handState.leftHandState = "login"
                        handState.rightHandState = None
						
                    BobstController.update_machine(handState)
                #else:
                    #print "Prout"
                        
        else:
            if len(handList)==2:
                    hand1=handList[0]
                    hand2=handList[1]

                    normal1=hand1.palm_normal
                    normal2=hand2.palm_normal
                    velocity1=hand1.palm_velocity
                    velocity2=hand2.palm_velocity
                    
                    if normal1[1]<0 and normal2[1]<0:
                        # print "Hand Down"
                        if velocity1[1]<-epsilon and velocity2[1]<-epsilon:
                            print "MIN"
                            handState.leftHandState = "minSpeed"
                            handState.rightHandState = "minSpeed"
                            BobstController.update_machine(handState)
                        #else:
                            #print "RIEN"

                    
                    elif normal1[1]>0 and normal2[1]>0 :
                        # print "Hand Up"
                        if velocity1[1]>epsilon and velocity2[1]>epsilon:
                            print "MAX"
                            handState.leftHandState = "maxSpeed"
                            handState.rightHandState = "maxSpeed"
                            BobstController.update_machine(handState)
                        #else:
                            #print "RIEN"
                    #else:
                        #print "RIEN"


            elif len(handList)==1:
                for hand in handList:
                    normal=hand.palm_normal
                    velocity=hand.palm_velocity
                    
                    # print velocity
                    if len(handList)==1:
                        if normal[1]<0:
                            # print "Hand Down"
                            if velocity[1]<-epsilon:
                                print "Speed -"
                                if(hand.is_right):
                                    handState.rightHandState = "negative"
                                    handState.leftHandState = None
                                elif(hand.is_left):
                                    handState.leftHandState = "negative"
                                    handState.rightHandState = None
                                BobstController.update_machine(handState)
                            #else:
                                #print "RIEN"
                        
                        else:
                            # print "Hand Up"
                            if velocity[1]>epsilon:
                                print "Speed +"
                                if(hand.is_right):
                                    handState.rightHandState = "positive"
                                    handState.leftHandState = None
                                elif(hand.is_left):
                                    handState.leftHandState = "positive"
                                    handState.rightHandState = None
                                BobstController.update_machine(handState)
                            #else:
                                #print "RIEN"
        
        #Francesca
        #print "Current FPS: ", frame.current_frames_per_second
        handslist = frame.hands

        
        #print "Hands detected: ", len(handslist)
        if self.insideApp == True:
            for hand in handslist:
                #detect closed hands
                if hand.sphere_radius < CONST_CLOSEDRADIUS and self.startCounting == 0:
                    self.startCounting = 1
                    if hand.is_right:
                        self.rightHand = 1
                    elif hand.is_left:
                        self.rightHand = 0
                if self.startCounting == 1 and self.rightHand == hand.is_right:
                    #print "Counting and on the selected hand"
                    if hand.sphere_radius < CONST_CLOSEDRADIUS:
                        #print "Incrementing"
                        self.closedHandCounter += 1
                    else:
                        #debug this: why is this never entered?
                        self.startCounting = 0
                        self.closedHandCounter = 0
                        print "Hand closing gesture interrupted"
                if self.closedHandCounter >= CONST_CLOSEDLENGTH and self.rightHand == hand.is_right:
                    self.closedHandCounter = 0
                    if(hand.is_right):
                        handState.rightHandState = "reset"
                    elif(hand.is_left):
                        handState.leftHandState = "reset"
                    BobstController.update_machine(HandState)
                    print "Hand closed for 100 frames!"
                #if 2 hands are in view and both are closed
                if len(handslist) == 2 and self.startCounting == 1 and self.rightHand != hand.is_right and hand.sphere_radius < CONST_CLOSEDRADIUS:
                    #print "Both hands closed"
                    self.startCountingBoth = 1
                    self.startCounting = 0
                    self.closedHandCounter = 0
                #if we should be counting the frames with both hands closed
                if self.startCountingBoth == 1:
                    #how many hands are closed?
                    for doubleHand in handslist:
                        if doubleHand.sphere_radius < CONST_CLOSEDRADIUS:
                            self.howManyClosedHands += 1
                            if(doubleHand.is_right):
                                self.handClosedR = 1
                            else:
                                self.handClosedL = 1
                    #both hands: count till reboot
                    #print "This many hands are closed: ", self.howManyClosedHands
                    if self.howManyClosedHands == 2:
                        self.howManyClosedHands = 0
                        self.closedBothHandsCounter += 1
                    #one hand: resume counting till reset (handled in other conditions)
                    elif self.howManyClosedHands == 1:
                        print "Double hand gesture interrupted, one hand still closed"
                        self.howManyClosedHands = 0
                        self.startCountingBoth = 0
                        self.closedBothHandsCounter = 0
                        self.startCounting = 1
                        if self.handClosedR == 1:
                            self.rightHand = 1
                        else:
                            self.rightHand = 0
                    #no hands are closed: no counter active
                    elif self.howManyClosedHands == 0:
                        print "No more hands are closed"
                        self.howManyClosedHands = 0
                        self.startCountingBoth = 0
                        self.closedBothHandsCounter = 0
                        self.startCounting = 0
                        self.closedHandCounter = 0
                if self.closedBothHandsCounter >= 2 * CONST_CLOSEDLENGTH:
                    self.closedBothHandsCounter = 0
                    handState.rightHandState = "reset"
                    handState.leftHandState = "reset"
                    self.insideApp = False
                    BobstController.update_machine(HandState)
                    print "Both hands closed for 100 frames!"
                

                #look for the push movement to feed the feeder
                self.normal = hand.palm_normal
                if hand.is_right and self.normal.x < -.7:
                    #print "Right hand is horizontal."
                    self.rightHandHorizontal = 1
                elif hand.is_right and self.normal.x >= -.7:
                    #print "Right hand is not horizontal."
                    self.rightHandHorizontal = 0
                self.velocity = hand.palm_velocity
                if self.rightHandHorizontal == 1 and hand.is_right and self.velocity.x < -500:
                    handState.rightHandState = "push"
                    handState.leftHandState = None
                    BobstController.update_machine(HandState)
                    print "Pushed!"



def main():
    # Create a sample listener and controller
    listener = SampleListener()
    controller = Leap.Controller()

    # Have the sample listener receive events from the controller
    controller.add_listener(listener)

    # Keep this process running until Enter is pressed
    print "Press Enter to quit..."
    try:
        sys.stdin.readline()
    except KeyboardInterrupt:
        pass
    finally:
        # Remove the sample listener when done
        controller.remove_listener(listener)

if __name__ == "__main__":
    main()