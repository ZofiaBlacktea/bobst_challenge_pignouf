###########
# Imports #
###########
import requests
import socket
import Gestures
from pprint import pprint
import json
import Leap
import BobstApi
########
# Main #
########


def main():
    host = "127.0.0.1"
    port = 8080

    # Create a sample listener and controller
    listener = SampleListener()
    controller = Leap.Controller()

    # Have the sample listener receive events from the controller
    controller.add_listener(listener)

    # Keep this process running until Enter is pressed
    print("Press Enter to quit...")
    try:
        sys.stdin.readline()
    except KeyboardInterrupt:
        pass
    finally:
        # Remove the sample listener when done
        controller.remove_listener(listener)


# %%
machine = BobstApi(https=False)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


def update_machine(hand_state):
    print("Left Hand State : {}, Right Hand State : {}".format(
        hand_state.leftHandState, hand_state.rightHandState))
    sock.connect((host, port))
    sock.send("get_eye_position")
    area = sock.recv(4096)
    sock.close()

    try:
        if hand_state.leftHandState == "reset" and hand_state.rightHandState == "reset":
            r = machine.reboot()
        elif (hand_state.leftHandState == "reset" and hand_state.rightHandState != "reset") or (hand_state.leftHandState != "reset" and hand_state.rightHandState == "reset"):
            r = machine.reset()
        elif hand_state.leftHandState == "login" or hand_state.rightHandState == "login":
            r = machine.login()
        elif (hand_state.leftHandState == "positive" and hand_state.rightHandState != "positive") or (hand_state.leftHandState != "positive" and hand_state.rightHandState == "positive") and area == "m":
            r = machine.increase_speed()
        elif (hand_state.leftHandState == "negative" and hand_state.rightHandState != "negative") or (hand_state.leftHandState != "negative" and hand_state.rightHandState == "negative") and area == "m":
            r = machine.decrease_speed()
        elif hand_state.leftHandState == "positive" or hand_state.rightHandState == "positive" and area == "l":
            r = machine.fix_issue()
        elif hand_state.leftHandState == "negative" or hand_state.rightHandState == "negative" and area == "l":
            r = machine.empty_delivery()
        elif hand_state.leftHandState == "positive" or hand_state.rightHandState == "positive" and area == "r":
            r = machine.feeder_start()
        elif hand_state.leftHandState == "negative" or hand_state.rightHandState == "negative" and area == "r":
            r = machine.feeder_stop()
        elif hand_state.leftHandState == "push" or hand_state.rightHandState == "push" and area == "r":
            r = machine.feeder_feed(100)
        elif hand_state.leftHandState == "maxpeed" and hand_state.rightHandState == "maxpeed" and area == "m":
            r = machine.get_state()
            while json.loads(r.text)["CurrentSpeed"] < 20:
                r = machine.set_speed(up=True)
        elif hand_state.leftHandState == "minspeed" and hand_state.rightHandState == "minspeed" and area == "m":
            r = machine.get_state()
            while json.loads(r.text)["CurrentSpeed"] > 5:
                r = machine.set_speed(up=False)
        r.raise_for_status()
        return r
    except (requests.exceptions.HTTPError, requests.exceptions.RequestException) as err:
        print(err)


if __name__ == "__main__":
    main()
