##########
# Import #
##########

import requests


class BobstApi:
    """
    Create a machine object to pilot the bobst machine via the GET interface.

    Attributes
    ----------
    base_url : str
        An url string that represents bobst server address

    https
        If True, uses a secure connection (default True)

    Methods
    -------
    get(request)
        Uses the requests library to pass GET requests to the server

    set_feeder(action, amount=100)
        Sets the feeder state: start, stop, feed (default 100)

    empty_delivery()
        Empties the delivery space

    set_speed(up=1)
        Sets the machine speed up or down (default up)

    get_state()
        Gets machine state

    reset()
        Resets the machine

    fix_issue()
        Fixes issue

    reboot()
        Reboots the machine

    login()
        Login user into the machine

    """

    def __init__(self, base_url="team-bes-simulator-lauzhack-bobst.azurewebsites.net"):
        self.base_url = base_url

        def empty_delivery(): return requests.get(self.baseurl + "/emptydelivery")
        def increase_seed(): return requests.get(self.baseurl + "/speedup")
        def decrease_seed(): return requests.get(self.baseurl + "/speeddown")
        def fix_issue(): return requests.get(self.baseurl + "/fixissue")
        def reboot(): return requests.get(self.baseurl + "/reboot")
        def login(): return requests.get(self.baseurl + "/login")
        def reset(): return requests.get(self.baseurl + "/reset")
        def feeder_start(): return requests.get(self.baseurl + "/startfeeder")
        def feeder_stop(): return requests.get(self.baseurl + "/stopfeeder")
        def feeder_feed(n):
        if n == None:
            raise ValueError("Need a value !")
        return requests.get("/feedfeeder?amount={}".format(n))
        def get_state(): return requests.get(self.baseurl + "/state").text
    """
    def get(self, request):
        try:
            if self.https:
                self.r = requests.get("https://"+self.base_url+request)
            else:
                self.r = requests.get("http://"+self.base_url+request)
            self.r.raise_for_status()
            return self.r
        except (requests.exceptions.HTTPError, requests.exceptions.RequestException) as err:
            print(err)

    def set_feeder(self, action, amount=100):

        if action == "start":
            self.request = "/startfeeder"
        elif action == "stop":
            self.request = "/stopfeeder"
        elif action == "feed":
            self.request = "/feedfeeder?amount={a}".format(a=amount)
        else:
            raise ValueError("Action is not valid")
        return self.get(request=self.request)

    def empty_delivery(self):
        return self.get(request="/emptydelivery")

    def set_speed(self, up=True):
        if up:
            return self.get(request="/speedup")
        else:
            return self.get(request="/speeddown")

    def get_state(self):
        return self.get(request="/state").text

    def reset(self):
        return self.get(request="/reset")

    def fix_issue(self):
        return self.get(request="/fixissue")

    def reboot(self):
        return self.get(request="/reboot")

    def login(self):
        return self.get(request="/login")
    """
