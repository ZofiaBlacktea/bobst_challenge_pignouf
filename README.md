# Bobst_Challenge_Pignouf

Control a bobst machine mockup with eye tracking and gestures using Leap motion

Team Members:
- Mathias DELAHAYE
- Thibault PORSSUT
- Zofia BLACKTEA
- Guillaume BROGGI

Machine simulator link:
https://team-bes-player-lauzhack-bobst.azurewebsites.net/
